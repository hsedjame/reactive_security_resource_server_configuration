package io.gitlab.hsedjame.project.security.resource.server.configuration.annotations;

import io.gitlab.hsedjame.project.security.resource.server.configuration.config.CorsFilter;
import io.gitlab.hsedjame.project.security.resource.server.configuration.config.WebFluxSecurityConfig;
import io.gitlab.hsedjame.project.security.resource.server.configuration.security.ResourceServerSecurityConfigBeanProvider;
import io.gitlab.hsedjame.project.security.resource.server.configuration.security.ReactiveResourceServerAuthenticationManager;
import io.gitlab.hsedjame.project.security.core.configuration.security.SecurityContextRepository;
import io.gitlab.hsedjame.project.security.core.exceptions.ConnectionExceptionMessages;
import io.gitlab.hsedjame.project.security.core.jwt.JwtProperties;
import io.gitlab.hsedjame.project.security.core.jwt.JwtUtils;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @Project MYALDOC_PORJECT
 * @Author Henri Joel SEDJAME
 * @Date 01/01/2019
 * @Class purposes : .......
 */

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({ResourceServerSecurityConfigBeanProvider.class,
        ConnectionExceptionMessages.class,
        SecurityContextRepository.class,
        ReactiveResourceServerAuthenticationManager.class,
        WebFluxSecurityConfig.class,
        CorsFilter.class,
        JwtUtils.class,
        JwtProperties.class})
public @interface EnableReactiveResourceServerSecurity {
}
