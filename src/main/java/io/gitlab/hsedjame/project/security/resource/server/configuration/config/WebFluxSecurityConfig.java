package io.gitlab.hsedjame.project.security.resource.server.configuration.config;

import io.gitlab.hsedjame.project.security.core.configuration.security.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

/**
 * @Project REACTIVE_SECURITY_JWT
 * @Author Henri Joel SEDJAME
 * @Date 31/12/2018
 * @Class purposes : .......
 */
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
public class WebFluxSecurityConfig {

    @Autowired
    private ReactiveAuthenticationManager authenticationManager;

    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http){
        return http
                .csrf().disable()
                .cors().configurationSource(SecurityUtils.corsConfigSource())
                .and()
                .formLogin().disable()
                .authenticationManager(authenticationManager)
                .securityContextRepository(SecurityUtils.securityContextRepository(authenticationManager))
                .authorizeExchange()
                    .anyExchange().authenticated()
                .and().build();
    }

}
