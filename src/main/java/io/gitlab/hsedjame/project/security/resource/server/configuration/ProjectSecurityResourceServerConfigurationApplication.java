package io.gitlab.hsedjame.project.security.resource.server.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"io.gitlab.hsedjame"})
public class ProjectSecurityResourceServerConfigurationApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectSecurityResourceServerConfigurationApplication.class, args);
    }

}
